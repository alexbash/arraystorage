package ru.alexbash;

import java.util.UUID;

public class ArrayStorage implements Storage<UUID, Resume> {

    private int capacity;
    private int size = 0;
    private Resume[] storage;

    public ArrayStorage(int capacity) {
        this.capacity = capacity;
        this.storage = new Resume[capacity];
    }


    @Override
    public boolean save(Resume resume) {
        if (size < capacity){
            storage[size] = resume;
            size++;
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public boolean delete(UUID uuid) {
        Resume r = get(uuid);
        int pos = 0;
        if (r.getUuid() != null && r.getUuid().equals(uuid)){
            for (int i = 0; i < size; i++) {
                if (storage[i].getUuid().equals(r.getUuid())) pos = i;
            }
            storage[pos] = storage[size-1];
            size--;
            return true;
        }
        return false;
    }

    @Override
    public Resume get(UUID obj) {
        for (int i = 0; i < size; i++) {
            if (storage[i].getUuid().equals(obj)) return storage[i];
        }
        return null; //
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean clear() {
        if (size != 0) {
            for (int i = 0; i < size; i++) {
                storage[i] = null;
                size = 0;
            }
            return true;
        }
        return false;
    }

    @Override
    public Resume[] getAll() {
        if (size != 0) {
            Resume[] resumes = new Resume[size];
            for (int i = 0; i < size; i++) {
                resumes[i] = storage[i];
            }
            return resumes;
        }
        return null;
    }
}
