package ru.alexbash;

public class MainArray {

    public static void main(String[] args) {
        Resume resume1 = new Resume("Alexey","Bashmakov","Programmer");
        Resume resume2 = new Resume("Maxim","Sokolov","Engineer");
        ArrayStorage storage = new ArrayStorage(10000);
        System.out.println("Result (1 or 0): " + storage.save(resume1));
        System.out.println("Result (1 or 0): " + storage.save(resume2));
        System.out.println("Size: " + storage.size());
        System.out.println("Resume by Alexey: " + storage.get(resume1.getUuid()).getUuid());
        System.out.println("Resume by Maxim: " + storage.get(resume2.getUuid()).getUuid());
        Resume [] array;
        array = storage.getAll();
        for (int i = 0; i < storage.size(); i++) {
            System.out.println("Item storage: " + array[i].getUuid());
        }
        System.out.println("Delete resume on UUID (1 or 0): " + storage.delete(array[1].getUuid()));
        System.out.println("New size: " + storage.size());
        for (int i = 0; i < storage.size(); i++) {
            System.out.println("Item storage: " + array[i].getUuid());
        }
        System.out.println("Clear storage: (1 or 0): " + storage.clear());
        System.out.println("Size past clear: " + storage.size());
        System.out.println("Item: " + storage.get(resume1.getUuid()));
        System.out.println("Item: " + storage.get(resume2.getUuid()));
    }

}

