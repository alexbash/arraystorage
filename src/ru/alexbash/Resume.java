package ru.alexbash;

import java.util.UUID;

public class Resume {

    private UUID uuid;
    private String firstname;
    private String lastname;
    private String spec;

    public Resume(String firstname, String lastname, String spec) {
        this.uuid = UUID.randomUUID();
        this.firstname = firstname;
        this.lastname = lastname;
        this.spec = spec;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }
}
