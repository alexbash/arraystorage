package ru.alexbash;

public interface Storage<T, U> {
    public boolean save(U obj);

    public boolean delete(T obg);

    public U get(T obj);

    public int size();

    public boolean clear();

    public U[] getAll();
}
